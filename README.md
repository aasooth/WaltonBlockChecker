﻿# WaltonBlockChecker is meant to help keep an eye on your Walton miners and help you know what is going on.

## Features:
- Async updating of all miner block heights
- New block alerting
- Scrapes 2 months back of your blocks on first run
- Estimates time until next block (to help keep your sanity)
- Hot reloading of the config file (so you don't have to restart the monitor)