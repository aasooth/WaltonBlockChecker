﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WaltonBlockChecker
{
        public class Config
    {
        public int MilisecondsInterval { get; set; }
        public String UsernameToSearchFor { get; set; }
    }

    public class RootObject
    {
        public List<Miner> Miners { get; set; }
        public Config Config { get; set; }
    }

    class WaltonBlockChecker
    {
        static int SleepTime = 30000;


        List<Miner> CurrentBlocks = new List<Miner>();

        public List<Miner> Instances = new List<Miner>();

        /// <summary>
        /// Gets the current block async.
        /// </summary>
        /// <param name="IP">The ip.</param>
        /// <returns></returns>
        public async Task<int> GetCurrentBlock(Miner IP)
        {
            var web3 = new Nethereum.Web3.Web3(IP.Address);

            var maxBlockNumber = await web3.Eth.Blocks.GetBlockNumber.SendRequestAsync();

            //Console.WriteLine(maxBlockNumber.Value);
            CurrentBlocks.Add(new Miner(IP.Name, (int)maxBlockNumber.Value));
            return (int)maxBlockNumber.Value;
        }

        /// <summary>
        /// Runs the checking loop. Written for recursive
        /// </summary>
        /// <returns></returns>
        public List<Miner> RunChecker()
        {
            List<Task> TaskList = new List<Task>();
            CurrentBlocks = new List<Miner>();

            foreach (Miner bob in Instances)
            {
                var task1 = GetCurrentBlock(bob);
                TaskList.Add(task1);
            }

            Task.WaitAll(TaskList.ToArray());

            List<Miner> Unique = CurrentBlocks.Distinct().ToList();
            return Unique;
        }

        public WaltonBlockChecker(Boolean Startup)
        {
            try
            {
                // Read in the config file
                using (StreamReader r = new StreamReader("config.json"))
                {
                    string json = r.ReadToEnd();
                    RootObject items = JsonConvert.DeserializeObject<RootObject>(json);

                    // Add the miners
                    foreach (Miner IP in items.Miners)
                    {
                        Instances.Add(IP);

                    }

                    // Set the config
                    if (items.Config.MilisecondsInterval > 0)
                    {
                        SleepTime = items.Config.MilisecondsInterval;

                        NewBlockChecker wbs = new NewBlockChecker();

                        if (Startup)
                        {
                            try
                            {
                                DateTime LastFoundAndRecorded = wbs.PreviouslyKnownBlocks.Blocks.Max(x => x.WhenMined);
                                int PagesBack = (int)((DateTime.Now - LastFoundAndRecorded).TotalSeconds / 30 / 24) + 5;
                                Console.WriteLine("Looks like you're doing a fresh launch of the program.");
                                Console.WriteLine("The last block we have on record is from " + LastFoundAndRecorded);
                                Console.WriteLine("Grabbing " + PagesBack + " pages of data to catch up!");
                                BlockContainer Catchup = wbs.FirstTimeRunBlockScanner(items.Config.UsernameToSearchFor, PagesBack);
                                //Catchup.Blocks.RemoveAll(x => wbs.PreviouslyKnownBlocks.Blocks.Any(y => y.BlockNumber == x.BlockNumber));
                                Catchup.Blocks.AddRange(wbs.PreviouslyKnownBlocks.Blocks);
                                Catchup.Blocks.Distinct().ToList();
                                wbs.SaveOutBlocks(Catchup);
                            }
                            catch (Exception ex)
                            {

                            }
                        }
                        else
                        {
                            wbs.Run(items.Config.UsernameToSearchFor);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Looks like there's something wrong with your config file..");
                Console.WriteLine(ex.Message);
                Console.ReadLine();
                System.Environment.Exit(1);
            }

            List<Miner> Unique = RunChecker();
            if (Unique.Count > 1)
            {
                // We need to wait 1 more cycle to see if they catch up
                Thread.Sleep(SleepTime);
                Unique = RunChecker();
                if (Unique.Count > 1)
                {
                    Console.WriteLine("One or more miners appear to be out of sync!");
                    foreach (Miner currnet in CurrentBlocks)
                    {
                        Console.WriteLine(currnet);
                    }
                }
            }
            else
            {
                Console.WriteLine(Instances.Count + " GPUs Last Checked: " + DateTime.Now);
            }

        }
        static void Main(string[] args)
        {
            // Startup run
            try
            {
                if (!File.Exists("KnownBlocks.json"))
                {
                    BlockContainer temp = new BlockContainer();
                    temp.Blocks = new List<BlockObject>();
                    string jsonData = JsonConvert.SerializeObject(temp, Formatting.None);
                    File.WriteAllText("./KnownBlocks.json", jsonData);
                }
                new WaltonBlockChecker(true);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            // Loop forever
            while (true)
            {
                try
                {
                    new WaltonBlockChecker(false);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                Thread.Sleep(SleepTime);
            }

            Console.ReadLine();
        }
    }
}
