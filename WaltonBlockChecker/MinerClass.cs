﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaltonBlockChecker
{
    public class Miner : IEquatable<Miner>
    {
        public string Address { get; set; }
        public string Name { get; set; }
        public int BlockNumber { get; set; }

        public Miner(string _name, int _blocknumber)
        {
            this.Name = _name;
            this.BlockNumber = _blocknumber;
        }

        public int CompareTo(Miner that)
        {
            return this.BlockNumber.CompareTo(that.BlockNumber);
        }

        public bool Equals(Miner CheckingAgainst)
        {
            //Check whether the compared object is null.  
            if (Object.ReferenceEquals(CheckingAgainst, null)) return false;

            //Check whether the compared object references the same data.  
            if (Object.ReferenceEquals(this, CheckingAgainst)) return true;

            //Check whether the UserDetails' properties are equal.  
            return CheckingAgainst.BlockNumber == this.BlockNumber;
        }

        public override int GetHashCode()
        {
            //Get hash code for the UserName field if it is not null.  
            int hashAddress = Address == null ? 0 : Address.GetHashCode();

            //Calculate the hash code for the GPOPolicy.  
            return hashAddress;
        }

        public override String ToString()
        {
            return Name + " - " + BlockNumber;
        }
    }
}
