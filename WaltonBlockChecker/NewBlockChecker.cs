﻿using HtmlAgilityPack;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WaltonBlockChecker
{
   
    class NewBlockChecker
    {
        static int SecondsBetweenIncomeStatement = 3600;
        static DateTime LastRepped;
        static int FirstTimeScanPagesBack = 1500;

        /// <summary>
        /// Returns a list of the latest Walton Blocks
        /// </summary>
        /// <returns></returns>
        public BlockContainer GetNewBlocks(String UserToSearchFor, String URL)
        {
            WebClient bob = new WebClient();
            String data = bob.DownloadString(URL);
            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(data);

            BlockContainer RecentBlocks = new BlockContainer();
            RecentBlocks.Blocks = new List<BlockObject>();

            foreach (HtmlNode table in doc.DocumentNode.SelectNodes("//table"))
            {
                foreach (HtmlNode row in table.SelectNodes("tr"))
                {
                    int counter = 0;
                    BlockObject current = new BlockObject();
                    foreach (HtmlNode cell in row.SelectNodes("th|td"))
                    {
                        if (counter == 0)
                        {
                            int num = 0;
                            int.TryParse(cell.InnerText.Trim(), out num);
                            current.BlockNumber = num;
                        }
                        else if (counter == 1)
                        {
                            current.GMNAddress = cell.InnerText.Trim();
                        }
                        else if (counter == 2)
                        {
                            current.ExtraData = cell.InnerText.Trim();
                        }
                        // Gas used, we don't care
                        else if (counter == 3)
                        {
                        }
                        else if (counter == 4)
                        {
                            DateTime stuff;
                            DateTime.TryParse(cell.InnerText.Trim() + "Z", out stuff);
                            current.WhenMined = stuff;
                        }
                        counter++;
                    }
                    // Don't add if bad data
                    if (current.BlockNumber > 0 && current.ExtraData.ToLower().Contains(UserToSearchFor.ToLower()))
                    {
                        RecentBlocks.Blocks.Add(current);
                        //Console.WriteLine(current.ToString());
                    }
                }
            }

            return RecentBlocks;

        }

        /// <summary>
        /// Scans FirstTimeScanPagesBack pages back to build up a list of your blocks
        /// </summary>
        /// <param name="UserToSearchFor">The user to search for.</param>
        /// <returns></returns>
        public BlockContainer FirstTimeRunBlockScanner(String UserToSearchFor, int PagesBack)
        {
            if (PagesBack == FirstTimeScanPagesBack)
                Console.WriteLine("Looks like this is your first time run. Collecting previous blocks.");

            BlockContainer FirstTimeScan = new BlockContainer();

            var progress = new ProgressBar();

            FirstTimeScan = GetNewBlocks(UserToSearchFor, "https://wtc.ycht.io/");

            for (int i = 0; i< PagesBack; i++)
            {
                BlockContainer NextPage = GetNewBlocks(UserToSearchFor, "https://wtc.ycht.io/?page="+i);
                FirstTimeScan.Blocks.AddRange(NextPage.Blocks);
                progress.Report((double)i / PagesBack);
                //Thread.Sleep(1000);
            }
            progress.Dispose();
            FirstTimeScan.Blocks = FirstTimeScan.Blocks.Distinct().ToList();
            return FirstTimeScan;
        }
        
        public void NewBlockAlert(List<BlockObject> BlocksToRep)
        {
            BlocksToRep = BlocksToRep.Distinct().ToList();
            Console.WriteLine("=======================================================================");
            Console.WriteLine("BLOCK FOUND!!!");
            foreach (BlockObject next in BlocksToRep)
            {
                Console.WriteLine(next.ToString());
            }
            Console.WriteLine("=======================================================================");
        }

        public void Run(String UserToSearchFor)
        {
            BlockContainer RecentlyMinedBlocks = GetNewBlocks(UserToSearchFor, "https://wtc.ycht.io/");
            BlockContainer RecentlyMinedBlocks2 = GetNewBlocks(UserToSearchFor, "https://wtc.ycht.io/?page=2");
            RecentlyMinedBlocks.Blocks.AddRange(RecentlyMinedBlocks2.Blocks);
            RecentlyMinedBlocks.Blocks = RecentlyMinedBlocks.Blocks.Distinct().ToList();

            // Remove already known blocks - will fail if this is first time run
            if (PreviouslyKnownBlocks != null)
            {
                if (PreviouslyKnownBlocks.Blocks != null && PreviouslyKnownBlocks.Blocks.Count != 0)
                {
                    // Check if it's time to Rep stats
                    if ((DateTime.Now - LastRepped).TotalSeconds > SecondsBetweenIncomeStatement)
                    {
                        double AverageTimeBetweenBlocks = 0.0;
                        int counter = 0;
                        for (int i = 0; i < PreviouslyKnownBlocks.Blocks.Count && i < 11; i++)
                        {
                            try
                            {
                                DateTime First = PreviouslyKnownBlocks.Blocks[i].WhenMined;
                                DateTime Second = PreviouslyKnownBlocks.Blocks[i + 1].WhenMined;
                                AverageTimeBetweenBlocks += (First - Second).TotalHours;
                                counter++;
                            }
                            catch
                            {
                                counter++;
                            }
                        }

                        AverageTimeBetweenBlocks = AverageTimeBetweenBlocks / counter;

                        Console.WriteLine("Average block found time of: " + AverageTimeBetweenBlocks.ToString("#.##") + " hours");
                        Console.WriteLine("Hopefully find a block in about: " + (((DateTime.Now - PreviouslyKnownBlocks.Blocks[0].WhenMined).TotalHours - AverageTimeBetweenBlocks) * -1).ToString("#.##") + " hours");

                        int Last24Hours = PreviouslyKnownBlocks.Blocks.Where(x => x.WhenMined > DateTime.Now.AddHours(-24) && x.WhenMined < DateTime.Now).ToList().Count;
                        Console.WriteLine(Last24Hours * 3.25 + " WTC earned in the last 24 hours");

                        LastRepped = DateTime.Now;
                    }

                    RecentlyMinedBlocks.Blocks.RemoveAll(x => PreviouslyKnownBlocks.Blocks.Any(y => y.BlockNumber == x.BlockNumber));

                    if (RecentlyMinedBlocks.Blocks.Count > 0)
                    {
                        SaveOutBlocks(RecentlyMinedBlocks);
                    }
                }
                // If we have no previous data - first time run
                else
                {
                    RecentlyMinedBlocks = FirstTimeRunBlockScanner(UserToSearchFor, FirstTimeScanPagesBack);
                    SaveOutBlocks(RecentlyMinedBlocks);
                }
            }
            // Something wrong with the known blocks file
            //RecentlyMinedBlocks = FirstTimeRunBlockScanner(UserToSearchFor, FirstTimeScanPagesBack);
            //SaveOutOnlyNewBlocks(RecentlyMinedBlocks);

        }

        public void SaveOutBlocks(BlockContainer RecentlyMinedBlocks)
        {
            NewBlockAlert(RecentlyMinedBlocks.Blocks.Distinct().ToList());
            try { RecentlyMinedBlocks.Blocks.AddRange(PreviouslyKnownBlocks.Blocks); } catch { }
            BlockContainer temp = new BlockContainer();
            temp.Blocks = RecentlyMinedBlocks.Blocks.Distinct().ToList();
            string jsonData = JsonConvert.SerializeObject(temp, Formatting.None);
            File.WriteAllText("./KnownBlocks.json", jsonData);
        }

        public BlockContainer PreviouslyKnownBlocks;

        public NewBlockChecker()
        {
            // Read in the previous blocks file
            PreviouslyKnownBlocks = new BlockContainer();

            try
            {
                using (StreamReader r = new StreamReader("./KnownBlocks.json"))
                {
                    string json = r.ReadToEnd();
                    PreviouslyKnownBlocks = JsonConvert.DeserializeObject<BlockContainer>(json);
                }
            }
            catch (Exception ex)
            {
                BlockContainer PreviouslyKnownBlocks = new BlockContainer();
                PreviouslyKnownBlocks.Blocks = new List<BlockObject>();
                SaveOutBlocks(PreviouslyKnownBlocks);
                Console.WriteLine(ex.Message);
            }
        }
    }
}
