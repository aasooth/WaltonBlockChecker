﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaltonBlockChecker
{
    [Serializable]
    class BlockContainer
    {
        public List<BlockObject> Blocks { get; set; }
    }

    [Serializable]
    class BlockObject : IComparable<BlockObject>
    {
        public int BlockNumber { get; set; }
        public String GMNAddress { get; set; }
        public String ExtraData { get; set; }
        public DateTime WhenMined { get; set; }
        public override String ToString()
        {
            return BlockNumber + " " + GMNAddress + " " + ExtraData + " " + WhenMined;
        }
        public int CompareTo(BlockObject that)
        {
            return this.BlockNumber.CompareTo(that.BlockNumber);
        }

        public bool Equals(BlockObject userDetail)
        {

            //Check whether the compared object is null.  
            if (Object.ReferenceEquals(userDetail, null)) return false;

            //Check whether the compared object references the same data.  
            if (Object.ReferenceEquals(this, userDetail)) return true;

            //Check whether the UserDetails' properties are equal.  
            return BlockNumber.Equals(userDetail);
        }
    }
}
